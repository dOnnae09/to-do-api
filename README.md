This is a .Net Core To Do API.

You can Create, Update, Delete, List, or View a task using this API.

The database is a SQL Server. 
---------------Here is the script to create the table on your SQL database------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Tasks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[Date] [date] NULL,
	[Completed] [bit] NULL,
 CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
---------------Here is the script to create the table on your SQL database------------------------

On the appsettings.json file, you can set the correct connection string for your SQL database.

The application is set to listen to http://localhost:2020

Here are the endpoints:

Get Task List
This gets all the task ordered by date
GET http://localhost:2020/tasks

Create a Task
POST http://localhost:2020/tasks
Body Parameter
{
  "title": "This is a sample task",
  "date": "08/26/2020",
  "completed": false
}

Update a Task
PUT http://localhost:2020/tasks/{id}
Updates a task details with the given ID.
Path Parameter
`id` - integer
Body Parameter
{
	"id" : 1,
  "title": "This is a sample task",
  "date": "08/26/2020",
  "completed": true
}

Delete a Task
DELETE http://localhost:2020/tasks/{id}
Deletes a task with the given ID.
Path Parameter
`id` - integer
