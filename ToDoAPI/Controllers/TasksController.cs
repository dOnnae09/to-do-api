﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoAPI.Models;
using ToDoAPI.Services;

namespace ToDoAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private ITasksService _tasksService;

        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public async Task<IActionResult> Tasks()
        {
            var taskList = await _tasksService.GetAllTasks();
            if (taskList.Any())
            {
                return Ok(taskList);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTaskById(int id)
        {
            var task = await _tasksService.GetTaskById(id);
            if (task != null)
            {
                return Ok(task);
            }
            else
            {
                return NotFound();
            }

        }

        [HttpPost]
        public async Task<IActionResult> CreateTask([FromBody] Tasks taskEntry)
        {
            var task = await _tasksService.CreateTask(taskEntry);
            if (task != null)
            {
                return Ok(task);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTask(int id, [FromBody] Tasks taskEntry)
        {
            if (id != taskEntry.Id)
            {
                return BadRequest();
            }

            var task = await _tasksService.UpdateTasks(taskEntry);
            if (task != null)
            {
                return Ok(task);
            }
            else
            {
                return NoContent();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTaskById(int id)
        {
            var isDeleted = await _tasksService.DeleteTaskById(id);
            if (isDeleted)
            {
                return Ok();
            }
            else
            {
                return NoContent();
            }
        }
    }
}
