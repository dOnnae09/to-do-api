﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoAPI.Models;

namespace ToDoAPI.Services
{
    public interface ITasksService
    {
        Task<Tasks> CreateTask(Tasks taskEntry);
        Task<bool> DeleteTaskById(int id);
        Task<IList<Tasks>> GetAllTasks();
        Task<Tasks> GetTaskById(int id);
        Task<Tasks> UpdateTasks(Tasks taskEntry);
    }
}