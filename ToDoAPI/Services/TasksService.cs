﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoAPI.Context;
using ToDoAPI.Models;

namespace ToDoAPI.Services
{
    public class TasksService : ITasksService
    {
        private TasksContext _context;

        public TasksService(TasksContext context)
        {
            _context = context;
        }

        public async Task<IList<Tasks>> GetAllTasks()
        {
            return await _context.Tasks.OrderBy(x => x.Date).ToListAsync();
        }

        public async Task<Tasks> GetTaskById(int id)
        {
            return await _context.Tasks.Where(x => x.Id == id).SingleOrDefaultAsync();
        }

        public async Task<Tasks> CreateTask(Tasks taskEntry)
        {
            _context.Tasks.Add(taskEntry);

            if (await _context.SaveChangesAsync() > 0)
            {
                return taskEntry;
            }
            else
            {
                return null;
            }
        }

        public async Task<Tasks> UpdateTasks(Tasks taskEntry)
        {
            _context.Entry(taskEntry).State = EntityState.Modified;

            if (await _context.SaveChangesAsync() > 0)
            {
                return taskEntry;
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> DeleteTaskById(int id)
        {
            var task = await _context.Tasks.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            if (task == null)
            {
                return false;
            }

            _context.Tasks.Remove(task);
            if (_context.SaveChanges() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
